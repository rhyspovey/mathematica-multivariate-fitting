(* ::Package:: *)

(* ::Title:: *)
(*Multivariate Fitting*)


(* ::Text:: *)
(*Rhys G. Povey <rhyspovey@gmail.com>*)


(* ::Text:: *)
(*This package is designed to flatten multivariate fit models into multivariable fit models that have a scalar response (LinearModelFit, GeneralizedLinearModelFit, NonlinearModelFit).*)


(* ::Text:: *)
(*Currently does not support full response (non-diagonal) covariance matrix input.*)


(* ::Text:: *)
(*29 June 2020*)
(*- Fixed context.*)
(**)
(*4 March 2020*)
(*- Fixed no weights bug.*)
(**)
(*27 February 2020*)
(*- First version.*)


(* ::Section:: *)
(*Front End*)


BeginPackage["MultivariateFitting`"];


VectorizeMultivariateData::usage="VectorizeMultivariateData[data,A] with data = {{\!\(\*SubscriptBox[OverscriptBox[\(x\), \(\[RightVector]\)], \(1\)]\),\!\(\*SubscriptBox[OverscriptBox[\(y\), \(\[RightVector]\)], \(1\)]\)},\[Ellipsis]} and A=B\[Transpose] such that each \!\(\*SubscriptBox[OverscriptBox[\(y\), \(\[RightVector]\)], \(i\)]\)=A.\!\(\*SubscriptBox[OverscriptBox[\(x\), \(\[RightVector]\)], \(i\)]\). Outputs {new data, param list} with new data = {{\!\(\*SubsuperscriptBox[\(x\), \(11\), \(\[Prime]\)]\),\!\(\*SubsuperscriptBox[\(x\), \(12\), \(\[Prime]\)]\),\[Ellipsis],\!\(\*SubsuperscriptBox[\(y\), \(1\), \(\[Prime]\)]\)},\[Ellipsis]}. Optional weights can be included with VectorizeMultivariateData[data,A,weights] where the weights = {\!\(\*SubscriptBox[OverscriptBox[\(w\), \(\[RightVector]\)], \(1\)]\),\!\(\*SubscriptBox[OverscriptBox[\(w\), \(\[RightVector]\)], \(2\)]\),\[Ellipsis]} correspond to the \!\(\*SubscriptBox[OverscriptBox[\(y\), \(\[RightVector]\)], \(i\)]\), if a single weight \!\(\*SubscriptBox[\(w\), \(i\)]\) is given it will be applied to all \!\(\*OverscriptBox[\(y\), \(\[RightVector]\)]\) coordinates.";


VectorizeMultivariateData::dimX="Dimension of design (independent varable) input is not uniform.";


VectorizeMultivariateData::dimY="Dimension of response (dependent varable) input is not uniform.";


VectorizeMultivariateData::weights="Weights not formatted correctly.";


VectorizeMultivariateMatrices::usage="VectorizeMultivariateMatrices[design,response,B] with design = X = {\!\(\*SubscriptBox[OverscriptBox[\(x\), \(\[RightVector]\)], \(1\)]\),\[Ellipsis]}, response = Y = {\!\(\*SubscriptBox[OverscriptBox[\(y\), \(\[RightVector]\)], \(1\)]\),\[Ellipsis]}, such that Y=X.B meaning each \!\(\*SubscriptBox[OverscriptBox[\(y\), \(\[RightVector]\)], \(i\)]\)=A.\!\(\*SubscriptBox[OverscriptBox[\(x\), \(\[RightVector]\)], \(i\)]\) for A=B\[Transpose]. Outputs {new design, new response, param vector}. Optional weights can be included with VectorizeMultivariateMatrices[design,response,B,weights] where the weights = {\!\(\*SubscriptBox[OverscriptBox[\(w\), \(\[RightVector]\)], \(1\)]\),\!\(\*SubscriptBox[OverscriptBox[\(w\), \(\[RightVector]\)], \(2\)]\),\[Ellipsis]} correspond to the \!\(\*SubscriptBox[OverscriptBox[\(y\), \(\[RightVector]\)], \(i\)]\), if a single weight \!\(\*SubscriptBox[\(w\), \(i\)]\) is given it will be applied to all \!\(\*OverscriptBox[\(y\), \(\[RightVector]\)]\) coordinates.";


VectorizeMultivariateMatrices::dimX="Dimension of design (independent varable) input is not uniform.";


VectorizeMultivariateMatrices::dimY="Dimension of response (dependent varable) input is not uniform.";


VectorizeMultivariateMatrices::weights="Weights not formatted correctly.";


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


VectorizeMultivariateData[data_List,paramT_List,weights_:Missing[]]:=Module[{ndata,dim1list,dim2list,dim1,dim2,design,newdesign,response,newresponse,newparam,newdata,newweights},
ndata=Length[data];

dim1list=Union[Length[#[[1]]]&/@data];
If[Length@dim1list==1,dim1=First@dim1list,Message[VectorizeMultivariateData::dimX];Return[$Failed]];

dim2list=Union[Length[#[[2]]]&/@data];
If[Length@dim2list==1,dim2=First@dim2list,Message[VectorizeMultivariateData::dimY];Return[$Failed]];

design=data[[All,1]];
response=data[[All,2]];

newdesign=Flatten[Table[Flatten/@(DiagonalMatrix[ConstantArray[\[FormalX],dim2]]/.{\[FormalX]->design[[\[FormalI]]],0->ConstantArray[0,dim1]}),{\[FormalI],ndata}],1];
newresponse=Flatten[response,1];
newparam=Flatten[paramT,1];
newdata=Flatten/@Join[{newdesign}\[Transpose],{newresponse}\[Transpose],2];

(* weights *)
(* if only one weight given for the vector y_i then apply it to all y_ij  *)
newweights=If[Not@MissingQ[weights],
Switch[Dimensions[weights],
{ndata,dim2},Flatten[weights,1],
{ndata},Flatten[ConstantArray[#,dim2]&/@weights,1],
_,Message[VectorizeMultivariateData::weights];Sequence[]
],
Missing[]
];

(* output *)
DeleteMissing[{newdata,newparam,newweights}]
];


VectorizeMultivariateMatrices[design_List,response_List,param_List,weights_:Missing[]]:=Module[{ndatalist,ndata,dim1list,dim2list,dim1,dim2,newdesign,newresponse,newparam,newweights},
ndatalist=Union[{Dimensions[design][[1]],Dimensions[response][[1]]}];
If[Length@ndatalist==1,ndata=First@ndatalist,Message[VectorizeMultivariateMatrices::ndata];Return[$Failed]];

dim1list=Union[{Dimensions[design][[2]],Dimensions[param][[1]]}];
If[Length@dim1list==1,dim1=First@dim1list,Message[VectorizeMultivariateMatrices::dimX];Return[$Failed]];

dim2list=Union[{Dimensions[response][[2]],Dimensions[param][[2]]}];
If[Length@dim2list==1,dim2=First@dim2list,Message[VectorizeMultivariateMatrices::dimY];Return[$Failed]];

newdesign=Flatten[Table[Flatten/@(DiagonalMatrix[ConstantArray[\[FormalX],dim2]]/.{\[FormalX]->design[[\[FormalI]]],0->ConstantArray[0,dim1]}),{\[FormalI],ndata}],1];
newresponse=Flatten[response,1];
newparam=Flatten[param\[Transpose],1];

(* weights *)
(* if only one weight given for the vector y_i then apply it to all y_ij  *)
newweights=If[Not@MissingQ[weights],
Switch[Dimensions[weights],
{ndata,dim2},Flatten[weights,1],
{ndata},Flatten[ConstantArray[#,dim2]&/@weights,1],
_,Message[VectorizeMultivariateMatrices::weights];Sequence[]
],
Missing[]
];

(* output *)
DeleteMissing[{newdesign,newresponse,newparam,newweights}]
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
