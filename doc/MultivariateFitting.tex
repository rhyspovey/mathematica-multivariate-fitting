\documentclass[letter,oneside,12pt]{article}

\usepackage[margin=2cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{mathabx}
\usepackage[all]{xy}

\newcommand{\modu}{\mathrm{mod}}
\newcommand{\quot}{\mathrm{quot}}

\setlength{\parskip}{10pt}
\setlength{\parindent}{0pt}

\begin{document}
\title{Multivariate Fitting}
\author{Rhys G. Povey}
\maketitle

%A multivariate fit function of the form $\vec{y}=\underline{\underline{A}} \cdot \vec{x}$ can be flattened by map $\mathcal{F}$ to a multivariable fit function as follows,
%\begin{align*}
%\xymatrix@M+5pt@R-30pt@C-50pt{
%**[l] \hat{\vec{y}} : \mathbb{R}^{d_1} \ar[r] & **[r] \mathbb{R}^{d_2} \\
%**[l] \vec{x} \ar@{|->}[r] & **[r] \vec{y} = \underline{\underline{B^\mathsf{T}}} \cdot \vec{x} \\
%**[l] \{x_i\}_{i=1}^{d_1} \ar@{|->}[r] \ar@{|->}@<8ex>[dd]^-{\mathcal{F}}& **[r] \{y_j = \sum_{i=1}^{d_1} B^\mathsf{T}_{\;j\,i}\, x_i \}_{j=1}^{d_2} \\
%**{+<0pt,20pt>}& \\
%**[l] \hat{y}^{\,\prime} : \mathbb{R}^{d_1 \times d_2} \ar[r] &**[r] \mathbb{R} \\
%**[l] \vec{x}^{\,\prime} \ar@{|->}[r] & **[r] y^{\,\prime} = \vec{x}^{\,\prime} \cdot \vec{b}^{\,\prime} \\
%**[l] \{x^{\,\prime}_\beta\}_{\beta=1}^{d_1 \times d_2} \ar@{|->}[r] & **[r] y^{\,\prime} = \sum_{\beta=1}^{d_1 \times d_2} x^{\,\prime}_\beta \, b^{\,\prime}_\beta
%}
%\end{align*}
Given a multivariate function of the form
\begin{align*}
\xymatrix@M+5pt@R-30pt@C-50pt{
**[l] \hat{\vec{y}} : \mathbb{R}^{d_1} \ar[r] & **[r] \mathbb{R}^{d_2} \\
**[l] \vec{x} \ar@{|->}[r] & **[r] \vec{y} = \underline{\underline{B^\mathsf{T}}} \cdot \vec{x} \\
**[l] \{x_i\}_{i=1}^{d_1} \ar@{|->}[r] & **[r] \{y_j = \sum_{i=1}^{d_1} B^\mathsf{T}_{\;j\,i}\, x_i \}_{j=1}^{d_2} \\
}
\end{align*}
the fit with a set of data points (independent-dependent variable pairs) $\{\vec{x}_k,\vec{y}_k\}$ can be flattened to a multivariable fit as follows
%Including a full set of data points (independent-dependent variable pairs) $\{\vec{x}_k,\vec{y}_k\}$ for $k=\{1,\dotsc,n\}$, this becomes
\begin{align*}
\xymatrix@M+5pt@R-30pt@C-80pt{
**[l] \hat{\underline{\underline{Y}}} : \bigoplus_{k=1}^n \mathbb{R}^{d_1} \ar[r] & **[r] \bigoplus_{k=1}^n \mathbb{R}^{d_2} \\
**[l] \underline{\underline{X}} \ar@{|->}[r] & **[r] \underline{\underline{Y}} = \underline{\underline{X}} \cdot \underline{\underline{B}} \\
**[l] \{x_{k\,i}\}_{k=1}^n,{}_{i=1}^{d_1} \ar@{|->}[r] \ar@{|->}@<8ex>[dd]^-{\mathcal{F}}& **[r] \{y_{k\,j} = \sum_{i=1}^{d_1} x_{k\,i}\,B_{i\,j}\, \}_{k=1}^n,{}_{j=1}^{d_2} \\
**{+<0pt,20pt>}& \\
**[l] \hat{\underline{y}}^{\,\prime} : \bigoplus_{\alpha=1}^{n \times d_2} \mathbb{R}^{d_1 \times d_2} \ar[r] & **[r] \mathbb{R}^{n \times d_2} \\
**[l] \underline{\underline{X}}^{\,\prime} \ar@{|->}[r] & **[r] \underline{y}^{\,\prime} = \underline{\underline{X}}^{\,\prime} \cdot \underline{b}^{\,\prime} \\
**[l] \{x^{\,\prime}_{\alpha\,\beta}\}_{\alpha=1}^{n \times d_2},{}_{\beta=1}^{d_1 \times d_2} \ar@{|->}[r] & **[r] \{y^{\,\prime}_\alpha = \sum_{\beta=1}^{d_1 \times d_2} x^{\,\prime}_{\alpha\,\beta} \, b^{\,\prime}_\beta \}_{\alpha=1}^{n \times d_2}
}
\end{align*}
If $\underline{b}^{\,\prime}$ is the list of parameters, then this is a linear model with design matrix $\underline{\underline{X}}^{\,\prime}$ and response $\underline{y}^{\,\prime}$.

The new vectors and matrix are obtained by flattening or rearranging,
\begin{align*}
x^{\,\prime}_{\alpha\,\beta} =&\; x_{\,(\alpha\,\quot_1\,d_2\, + 1) \; (\beta\,\modu_1\,d_1)}\;\delta_{\,(\alpha\,\modu_1\,d_2) \; (\beta\,\quot_1\,d_1\,+1)} \;, \\
y^{\,\prime}_\alpha =&\; y_{\,(\alpha\,\quot_1\,d_2\, + 1) \; (\alpha\,\modu_1\,d_2)} \;, \\
b^{\,\prime}_\beta =&\; b_{\,(\beta\,\quot_1\,d_2\, + 1) \; (\beta\,\modu_1\,d_2)} \;,
\end{align*}
where $m\,\quot_\Delta\,n$ is quotient operator with offset $\Delta$ such that it returns $z\;:\;\Delta\leq m-n\,z<\Delta+n$,
and $m\,\modu_\Delta\,n$ is modulus operator with offset $\Delta$ such that it returns $z\;:\;\Delta\leq z<\Delta+n$.

For consistency, the indices follow
\begin{align*}
i&\;\in[1,d_1] \subset\mathbb{Z} && \text{coordinate in $\mathbb{R}^{d_1}$}\;, \\
j&\;\in[1,d_2] \subset\mathbb{Z} && \text{coordinate in $\mathbb{R}^{d_2}$}\;, \\
k&\;\in[1,n] \subset\mathbb{Z} && \text{data point number}\;, \\
\alpha&\;\in[1,n \times d_2] \subset\mathbb{Z} \;, \\
\beta&\;\in[1,d_1 \times d_2] \subset\mathbb{Z} \;.
\end{align*}
\end{document}